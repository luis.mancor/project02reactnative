/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React,{Component} from 'react';

import {
  StyleSheet, 
  TouchableOpacity, 
  Text, 
  View, 
  Image,
  TextInput,
} from 'react-native';
import Message from './app/components/message/Message';



export default class App extends Component{

  constructor(props){
    super(props);
    this.state={
      textValue:'',
      count:0,
    }
  }

  changeTextInput = text=>{
    console.log(text)
    this.setState({textValue:text});
  };

  render(){
    return(
      <View style={styles.container}>
        <Message />
        <View style={styles.text}>
          <Text>Ingrese su edad</Text>
        </View>
        <TextInput
          style={{height:40, width:100, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
        />
      </View>
      
    );
  }

}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    paddingHorizontal:10,

  },
  text:{
    alignItems:'center',
    padding:10,
  },

  button:{
    top:10,
    alignItems:'center',
    backgroundColor:'#d7f8ff',
    padding:10,
    borderColor:'#3e4444',    
  },

  countCointainer:{
    alignItems:'center',
    padding:10,
  },
  countText:{
    color:'#6b5b95',
  }

});


